.. VideoLAN documentation index documentation master file, created by
   sphinx-quickstart on Sat Feb  1 15:23:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VideoLAN documentation!
========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

You can browse:
 - `VLC User Documentation <vlc-user/>`_
 - `VLC Developer Documentation <https://epirat.videolan.me/devdocs/>`_
